﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // basic tutorial c#. if u want to view the result of the function below here just copy and past function in this section.
            //ex. IsCheckNull(); and watch the result in console.
            
        }

        public static void TupleValue()
        {
            // tuple type with pass by value
            var baseTuple = ("lol", "lol2", "lol3"); //value

            string[] array = new[] { "string", "string2", "string3" }; //ref

            List<string> listString = new() { "list1", "list2", "list3" }; //ref

            WormHole(array, baseTuple, listString);

            Console.WriteLine($"out Worm Hole {array[0]} , {baseTuple} , {listString[3]}");
        }

        public static void WormHole(string[] stringArray , ValueTuple<string , string , string> tuple , List<string> listString)
        {
            string[] arrayW = stringArray;

            var wTuple = tuple;

            arrayW[0] = "string4";

            wTuple.Item1 = "outTe";

            List<string> wList = listString;

            wList.Add("Four");

            Console.WriteLine($"in Worm hole {arrayW[0]} , {wTuple} , {wList[3]}");
        }

        public static void SwitchCaseIsHere()
        {
            //new Switch case
            int switchNumber = 5;
            // ordinary
            switch (switchNumber)
            {
                case 0: break;
                default:
                    Console.WriteLine("default switch case");
                    break;
            }

            // new statement
            Console.WriteLine(GetWordByInt(switchNumber));
        }

        public static string GetWordByInt(int number) =>
            number switch
            {
                5 => "it a five" ,
                _ => "default case",
            };

        public static void IsCheckNull()
        {
            // 7.0 support unity
            //easy check null
            int? maybe = null;
            if (maybe is int number)
            {
                Console.Write("it is int number");
            }
            else
            {
                Console.Write("The nullable int 'maybe' doesn't hold a value");
            }
        }

        public static void IndicesAndRange()
        {
            // indice and ranges #8.0
            Range phrase = ^4..^0; // ^1 = end of the word = "man" => ^4..^0 mean - is , not , a , man "end";
            var word = new string[] { "the", "jaggod", "is", "not", "a", "man" };
            for (int i = 1; i < word[phrase].Length; i++)
            {
                Console.WriteLine(word[phrase][i]);
            }
        }

        public static void Fs()
        {
            //file stream read byte
            FileStream f = new FileStream("D:/expLearning/FileTestIO/steaming.txt", FileMode.Open, FileAccess.Read, FileShare.Read);

            for (int i = 0; i < f.Length; i++)
            {
                Console.Write(f.ReadByte() + " ");
            }

            Console.WriteLine();
            //file stream read text
            string text = File.ReadAllText("D:/expLearning/FileTestIO/steaming.txt");

            Console.WriteLine($"Read from text -- {text}");
        }

        public static void StringInterpolation()
        {
            //eazy 
            string ant = "ant";
            Console.Write($"ant is {ant} , and \"I am legent \" -> by will smith.");

            double spol = 11299792.4581613;//test number.

            FormattableString message = $"The speed of light is {spol:N4} km/s.";

            string messageInInvariantCulture = FormattableString.Invariant(message);

            Console.WriteLine("");
            Console.WriteLine($"this is message {message}");
            Console.WriteLine($"this is message InVariantCulture {messageInInvariantCulture}");
        }

        public static void CallQueryExpression()
        {
            int[] numbers = new int[8] { 1, 2, 9, 13, 4, 5, 9, 8 };

            string result = Console.ReadLine();

            int condition = int.Parse(result);

            QueryExpression queryExpression = new();

            queryExpression.HighNumberQuery(numbers, condition);
        }
    }
}
