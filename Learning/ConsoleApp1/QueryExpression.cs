﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    // version 3
    public class QueryExpression
    {
        // start query expression with IEnumerable type

        /// <summary>
        /// Method <c>HighNumberQuery</c> basic query, use to query number that have value more then condition number in array.
        /// </summary>
        /// <param name="numbers">the target array of int.</param>
        /// <param name="condition">the condition for where query in array.</param>


        public void HighNumberQuery(int[] numbers , int condition)
        {
            IEnumerable<int> highNumberQuery =
                from number in numbers
                where number > condition
                orderby number descending
                select number;

            foreach(int number in highNumberQuery)
            {
                Console.Write($"{number} ");
            }
        }
    }
}
